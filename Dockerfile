FROM mozilla/sops:latest AS lrzip

WORKDIR /tmp

RUN mkdir -p /lrz && \
    apt-get update && \
    apt-get install -y autoconf make libtool \
        libbz2-dev liblzo2-dev liblz4-dev libgpg-error-dev libgcrypt-dev && \
    apt-get autoremove -y && \
    apt-get clean && \
    rm -rvf /var/lib/{cache,log}/* /var/lib/{apt,dpkg}/lists/*

RUN git clone --recurse-submodules https://github.com/pete4abw/lrzip-next && \
    cd lrzip-next && \
    ./autogen.sh && \
    ./configure --prefix=/lrz && \
    make -j4 && \
    make install

FROM mozilla/sops:latest

COPY --from=lrzip /lrz /usr/local

RUN apt-get update && \
    apt-get install -y bzip2 liblzo2-2 tree curl vim gnupg2 make && \
    curl -L https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64/kubectl \
        -o /usr/local/bin/kubectl && \
    chmod +x /usr/local/bin/kubectl && \
    useradd -ms /bin/bash sops && \
    apt-get autoremove -y && \
    apt-get clean && \
    rm -rvf /var/lib/{cache,log}/* /var/lib/{apt,dpkg}/lists/*

USER sops
WORKDIR /home/sops

COPY ./entrypoint /root/entrypoint
COPY ./entrypoint.sh /entrypoint.sh

ENTRYPOINT [ "/entrypoint.sh" ]
