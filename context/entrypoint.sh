#!/usr/bin/env bash
if [ ! -d "$_ENTRYPOINT_ROOT" ]; then
    if [ -d "$HOME/entrypoint" ]; then
        eval "$( $HOME/entrypoint/bin/entrypoint init - )"
    else
        find "$( pwd )" -type s -name entrypoint -exec eval "\$( {} init - )" \;
    fi
fi

exec entrypoint $@
