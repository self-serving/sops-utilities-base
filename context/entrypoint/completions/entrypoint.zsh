if [[ ! -o interactive ]]; then
    return
fi

compctl -K _entrypoint entrypoint

_entrypoint() {
  local word words completions
  read -cA words
  word="${words[2]}"

  if [ "${#words}" -eq 2 ]; then
    completions="$(entrypoint commands)"
  else
    completions="$(entrypoint completions "${word}")"
  fi

  reply=("${(ps:\n:)completions}")
}
